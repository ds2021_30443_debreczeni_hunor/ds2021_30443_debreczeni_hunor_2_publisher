package ro.tuc.model;

import lombok.Data;

import java.util.UUID;

@Data
public class MonitoredDataDTO {
    private Long timestamp;
    private UUID sensor_id;
    private Double measurement_value;

    public MonitoredDataDTO(Long timestamp, UUID sensor_id, Double measurement_value) {
        this.timestamp = timestamp;
        this.sensor_id = sensor_id;
        this.measurement_value = measurement_value;
    }
}