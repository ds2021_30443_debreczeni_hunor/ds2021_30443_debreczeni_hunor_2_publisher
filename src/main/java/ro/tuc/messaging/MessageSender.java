package ro.tuc.messaging;

import java.io.IOException;

public interface MessageSender extends AutoCloseable {
    <T> void send(String queueName, T obj) throws IOException;
}
